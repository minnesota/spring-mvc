package com.minesota.service.impl;

import com.minesota.entity.UserEntity;
import com.minesota.service.UserService;
import com.minesota.dao.UserDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UserServiceImpl implements UserService{
    private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
    @Autowired
    private UserDao userDao;
    @Override
    public List<UserEntity> test(Integer id) {
        logger.info("id:{}",id);
       return userDao.select();
    }

    @Override
    @Transactional()
    public void insert(String name) {
        //事务 这里的insert会失败
        //不启用事务，则会插入成功
        userDao.insert(name);
        System.out.println(1/0);
    }
}
