package com.minesota.service;

import com.minesota.entity.UserEntity;

import java.util.List;

public interface UserService {
    List<UserEntity> test(Integer id);
    void insert(String name);
}
