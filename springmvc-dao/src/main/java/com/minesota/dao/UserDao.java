package com.minesota.dao;

import com.minesota.entity.UserEntity;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserDao {
    List<UserEntity> select();
    void insert(String name);
}
