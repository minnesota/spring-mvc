package com.minesota.filters;

import com.minesota.annotations.ServletApplication;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@ServletApplication
public class ChartSetFilter implements Filter{

    private String chartSet;

    public String getChartSet() {
        return chartSet;
    }

    @Override
    public void init(FilterConfig filterConfig){
       String chartSet= filterConfig.getInitParameter("chartSet");
       this.chartSet = chartSet;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest)servletRequest;
        httpServletRequest.setCharacterEncoding(this.getChartSet());
        HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;
        httpServletResponse.setHeader("Content-type", "text/html;charset="+chartSet);
        httpServletResponse.setCharacterEncoding(chartSet);
        filterChain.doFilter(httpServletRequest,httpServletResponse);
    }

    @Override
    public void destroy() {

    }
}
