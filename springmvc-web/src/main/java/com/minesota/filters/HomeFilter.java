package com.minesota.filters;

import com.minesota.annotations.ServletApplication;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@ServletApplication
@WebFilter(value = "/home")
public class HomeFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        Object loginId = httpServletRequest.getSession().getAttribute("loginId");
        if(null==loginId||"".equals(loginId.toString())){
            //先跳转到登陆界面
            httpServletRequest.getRequestDispatcher("/login").forward(servletRequest,servletResponse);
        }else{
            filterChain.doFilter(servletRequest,servletResponse);
        }
    }

    @Override
    public void destroy() {

    }

}
