package com.minesota.controller;

import com.alibaba.fastjson.JSONObject;
import com.minesota.entity.UserEntity;
import com.minesota.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/test")
public class TestController {
    private static final Logger logger = LoggerFactory.getLogger(TestController.class);
    @Autowired
    private UserService userService;

    /**
     * 标准的REST风格测试
     * @param id
     * @param name
     * @return
     */
    @RequestMapping(value = "/{id}/{name}",method = RequestMethod.GET)
    @ResponseBody
    public String get(@PathVariable Integer id,@PathVariable String name){
        //前端访问方式：http://localhost:8080/springmvc/test/1/andy
        logger.info("id:{},name:{}",id,name);
        List<UserEntity> userEntityList = userService.test(id);
        logger.info("用户列表:{}", JSONObject.toJSON(userEntityList));
        return "controller success!";
    }

    /**
     * 用于事务测试
     * @param name
     * @return
     */
    @RequestMapping(value = "/{name}",method = RequestMethod.PUT)
    @ResponseBody
    public String put(@PathVariable()String name){
        userService.insert(name);
        List<UserEntity> userEntityList = userService.test(0);
        logger.info("用户列表:{}", JSONObject.toJSON(userEntityList));
        return "success";
    }
}
