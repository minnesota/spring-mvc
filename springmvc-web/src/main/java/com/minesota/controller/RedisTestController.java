package com.minesota.controller;

import com.minesota.utils.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/redis")
public class RedisTestController {
    @Autowired
    private RedisUtil redisUtil;
    @ResponseBody
    @RequestMapping(value = "/{key}/{value}",method = RequestMethod.PUT)
    public String setTest(@PathVariable String key, @PathVariable String value){
        redisUtil.set(key,value);
        return "Success";
    }
    @ResponseBody
    @RequestMapping(value = "/{key}",method = RequestMethod.GET)
    public String getTest(@PathVariable String key){
        return redisUtil.get(key);
    }
}
