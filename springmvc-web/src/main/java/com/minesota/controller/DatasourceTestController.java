package com.minesota.controller;

import com.alibaba.druid.pool.DruidDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

@Controller
@RequestMapping("/datasource")
public class DatasourceTestController{
    private static final Logger logger = LoggerFactory.getLogger(DatasourceTestController.class);
    @Resource(name = "dataSource")
    private DruidDataSource druidDataSource;
    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public String test(){
        logger.info("username:{},password:{}",druidDataSource.getUsername(),druidDataSource.getPassword());
        return "success";
    }
}
