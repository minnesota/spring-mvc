package com.minesota.listener;

import com.minesota.annotations.ServletApplication;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
/**
 * 在线人数统计
 */
@ServletApplication
public class OnlineCountListener implements HttpSessionListener{

    @Override
    public void sessionCreated(HttpSessionEvent se) {
        //新增在线
       countAndRecord(true,se);
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
        //移除在线
       countAndRecord(false,se);
    }

    private void countAndRecord(boolean isOnline,HttpSessionEvent se){
        HttpSession httpSession =  se.getSession();
        ServletContext servletContext =  httpSession.getServletContext();
        Object record = servletContext.getAttribute("onlineCount");
        int onlineUserCount = 0;
        if(null!=record){
            onlineUserCount = Integer.valueOf(String.valueOf(record));
        }
        if(isOnline){
            onlineUserCount++;
        }else{
            if(onlineUserCount>0){
                onlineUserCount--;
            }
        }
        servletContext.setAttribute("onlineCount",onlineUserCount);
        System.out.println("当前在线人数:"+onlineUserCount);
    }
}
