package com.minesota.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.concurrent.TimeUnit;

public class RedisUtil {
    @Autowired
    private RedisTemplate<String,String> redisTemplate;
    private static final long expire_time = 60*24;//默认保存一天
    public boolean set(String key,String value){
        try{
            redisTemplate.opsForValue().set(key,value,expire_time, TimeUnit.MINUTES);
        }catch (Exception ex){
            return false;
        }
        return true;
    }
    public boolean set(String key,String value,long expireTime){
        try{
            redisTemplate.opsForValue().set(key,value,expireTime, TimeUnit.MINUTES);
        }catch (Exception ex){
            return false;
        }
        return true;
    }

    public String get(String key){
        try{
            return redisTemplate.opsForValue().get(key);
        }catch (Exception ex){
            return null;
        }
    }
}
